
## 0.0.10 [08-31-2024]

deprecate the Pre-Built

See merge request itentialopensource/pre-built-automations/versa-create-new-organization!12

2024-08-31 20:42:12 +0000

---

## 0.0.9 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/versa-create-new-organization!9

---

## 0.0.8 [03-03-2023]

* Updated readme and workflow

See merge request itentialopensource/pre-built-automations/versa-create-new-organization!8

---

## 0.0.7 [12-21-2021]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/versa-create-new-organization!4

---

## 0.0.6 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/versa-create-new-organization!3

---

## 0.0.5 [07-06-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/versa-create-new-organization!2

---

## 0.0.4 [05-25-2021]

* Patch/dsup 904 readme

See merge request itentialopensource/pre-built-automations/staging/versa-create-new-organization!1

---

## 0.0.3 [03-25-2021]

* Patch/dsup 904 readme

See merge request itentialopensource/pre-built-automations/staging/versa-create-new-organization!1

---

## 0.0.2 [03-19-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n

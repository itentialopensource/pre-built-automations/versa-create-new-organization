## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-01-2024 and will be end of life on 09-01-2025. The capabilities of this Pre-Built have been replaced by the [Versa - REST](https://gitlab.com/itentialopensource/pre-built-automations/versa-rest)

<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Versa Create New Organization

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This pre-built is used to create a new organization in Versa Director.
<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--

-->
<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 1 minute

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Itential Automation Platform
  * `^2022.1`
* A running instance of the Itential OpenSource Versa Director adapter, which can be installed from [here](https://gitlab.com/itentialopensource/adapters/controller-orchestrator/adapter-versa_director).

## Features

The main benefits and features of the Pre-Built are outlined below.

Every Itential Pre-built is designed to optimize network performance and configuration by focusing on reliability, flexibility and coordination of network changes with applications and IT processes. As the network evolves, Pre-builts allow customers to focus on effective management and compliance to minimize risks of disruption and outage that can negatively impact quality of service.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## How to Install

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:


The pre-built can be run as a standalone or as part of a parent job. 
- To run it as a standalone job, run the respective job from Operations Manager with the necessary json form. 
- The workflow or the job can also be used as part of another parent job.

The description of the formdata needed by the prebuilt is given below:
The required values to be passed on are:

**Organization Name:** The name of the organization to be created.\
**Parent organization:** The paret organization under which the current organization is to be created.\
**IKE authentication:** Select the type of Authentication to be used for IKE.\
**Shared control plane:** Select if same control plane will be used for all the control IPSec tunnels with the Controller for a multi-tenant CPE. Not enabled by default.\
**LAN routing Instance VPN enabled:** Enable automatic Data tunnels between branches / or with hubs. Enabled by default. Disable for Managed Router type deployment.\
**CPE Deployment type:** Select the type of CPE to be deployed. Selection between SD-WAN and vCPE available. SD-WAN is enabled by Default.\
**CMS connectors:** Connectors to external systems such as Public Cloud, OpenStack, vCloud Director for Organization & resource management. Select all the connectors required so that applicable resources are selected for onboarding devices in those environments.\
**Analytics clusters:** Select the analytics clusters that will be used to send availability and usage metrics from the CPEs.\
**Supported User roles:** Select the RBAC controls required for this organization.

**Controller Name:** Select all the controllers to which the device will establish control tunnels to.

<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Versa instance

The above created organization can be verified on the versa instance by logging in using the credentials provided.\
After logging in, select the workflows tab, choose 'Infrastructure' and then 'Organizations' from the left pane to view all the organizations created.

Click on the organization created to verify all the varibles such as Controllers, CMS connectors etc,.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
